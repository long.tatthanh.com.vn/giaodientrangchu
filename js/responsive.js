$(function() {
    $(".khungAnhCrop img")
        .each(function() {
            $(this)
                .removeClass("wide tall")
                .addClass((this.width / this.height > $(this).parent().width() / $(this).parent().height()) ?
                    "wide" :
                    "tall");
        });

    var slider_h = $('#slider').outerHeight();
    var c = slider_h - 30;
    $('ol.carousel-indicators').css('top', c + 'px');

    $('div.line-wrap').click(function() {
        $('div#nav').toggle(400);
    });

    $('.btn-datphong').click(function(){
    	$('#datphong').toggle(300);
    });


    // height và width khung ảnh
    var lgvw = $('.lg-video').outerWidth();
    var lgvh = $('.lg-video').outerHeight();
    //height và width của icon
    var icw = $('.icon-playvideo').outerWidth();
    var ich = $('.icon-playvideo').outerHeight();
    console.log(lgvw + "/ " + lgvh);
    // check
    var top = (lgvh / 2) - (ich / 2);
    var left = (lgvw / 2) - (icw / 2);

    $('.lg-video .icon-playvideo').css({
        'top': top + 'px',
        'left': left + 'px'

    });
    // height và width khung ảnh
    var smw = $('.video').outerWidth();
    var smh = $('.video').outerHeight();
    console.log(smw + "/ " + smh);
    //height và width của icon
    var icwsm = $('.icon-playvideo-sm').outerWidth();
    var ichsm = $('.icon-playvideo-sm').outerHeight();
    // check
    var t = (smh / 2) - (ichsm / 2);
    var l = (smw / 2) - (icwsm / 2);

    $('.sm-video .icon-playvideo-sm').css({
        'top': t + 'px',
        'left': l + 'px'

    });
    //lay chieu cao của khung - chieu cao cua 2 thu vien img va video
    var hqc = $('.quangcao').outerHeight();
    var thuvien = $('.thuvien').outerHeight();
    var margin_top = ((hqc / 2) - thuvien);
    if (margin_top > 0) {
        $('.lib').css({
            'margin-top': margin_top + 'px'
        })
    }
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var height = (window.innerHeight - 10);
        if (document.body.scrollTop > height || document.documentElement.scrollTop > height) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }
    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    $(function() {
        $('#ngayden').datetimepicker({
            viewMode: 'days',
            format: 'DD/MM/YYYY'
        });
        $('#ngaydi').datetimepicker({
            viewMode: 'days',
            format: 'DD/MM/YYYY'
        });
    });
    //google
    var gmap = new google.maps.LatLng(18.797835, 105.730549);
    var marker;

    function initialize() {
        var mapProp = {
            center: new google.maps.LatLng(18.797835, 105.730549),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        var styles = [{
            featureType: 'road.arterial',
            elementType: 'all',
            stylers: [{
                hue: '#fff'
            }, {
                saturation: 100
            }, {
                lightness: -48
            }, {
                visibility: 'on'
            }]
        }, {
            featureType: 'road',
            elementType: 'all',
            stylers: [

            ]
        }, {
            featureType: 'water',
            elementType: 'geometry.fill',
            stylers: [{
                color: '#adc9b8'
            }]
        }, {
            featureType: 'landscape.natural',
            elementType: 'all',
            stylers: [{
                hue: '#809f80'
            }, {
                lightness: -35
            }]
        }];

        var styledMapType = new google.maps.StyledMapType(styles);
        map.mapTypes.set('Styled', styledMapType);

        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: gmap
        });
        google.maps.event.addListener(marker, 'click', toggleBounce);
    }

    function toggleBounce() {

        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
})

$(window).resize(function() {
	$('btn-datphong')


    var slider_h = $('#slider').outerHeight();
    var c = slider_h - 30;
    $('ol.carousel-indicators').css('top', c + 'px');
    //video
    //// height và width khung ảnh
    var lgvw = $('.lg-video').outerWidth();
    var lgvh = $('.lg-video').outerHeight();
    //height và width của icon
    var icw = $('.icon-playvideo').outerWidth();
    var ich = $('.icon-playvideo').outerHeight();
    console.log(lgvw + "/ " + lgvh);
    // check
    var top = (lgvh / 2) - (ich / 2);
    var left = (lgvw / 2) - (icw / 2);

    $('.lg-video .icon-playvideo').css({
        'top': top + 'px',
        'left': left + 'px'

    });
    // height và width khung ảnh
    var smw = $('.video').outerWidth();
    var smh = $('.video').outerHeight();
    console.log(smw + "/ " + smh);
    //height và width của icon
    var icwsm = $('.icon-playvideo-sm').outerWidth();
    var ichsm = $('.icon-playvideo-sm').outerHeight();
    // check
    var t = (smh / 2) - (ichsm / 2);
    var l = (smw / 2) - (icwsm / 2);

    $('.sm-video .icon-playvideo-sm').css({
        'top': t + 'px',
        'left': l + 'px'

    });
});